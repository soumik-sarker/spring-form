package net.therap.springform.controller;

import net.therap.springform.command.ApplicationCommand;
import net.therap.springform.editor.AdminDataEditor;
import net.therap.springform.editor.DateEditor;
import net.therap.springform.editor.PointsEditor;
import net.therap.springform.model.Admin;
import net.therap.springform.utils.AdminListManager;
import net.therap.springform.validator.ApplicationCommandValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * @author soumik.sarker
 * @since 10/7/21
 */
@Controller
public class HomePageController {
}
